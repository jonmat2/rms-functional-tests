const { expect, matchTemplate, MatchStyle } = require('@aws-cdk/assert');
const cdk = require('@aws-cdk/core');
const RmsTestPipepline = require('../lib/rms_test_pipepline-stack');

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new RmsTestPipepline.RmsTestPipeplineStack(app, 'MyTestStack');
    // THEN
    expect(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
