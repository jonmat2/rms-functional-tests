const cdk = require("@aws-cdk/core");
const codebuild = require("@aws-cdk/aws-codebuild");
const codepipeline = require("@aws-cdk/aws-codepipeline");
const codepipeline_actions = require("@aws-cdk/aws-codepipeline-actions");
const iam = require("@aws-cdk/aws-iam");
const sns = require("@aws-cdk/aws-sns");
const subscriptions = require("@aws-cdk/aws-sns-subscriptions");
const cdk_developer_tools_notifications_1 = require("@cloudcomponents/cdk-developer-tools-notifications");
const bucket = require('@aws-cdk/aws-s3');

const {
  PipelineNotificationRule,
  PipelineEvent,
  SnsTopic,
} = require('@cloudcomponents/cdk-developer-tools-notifications');


class RmsTestPipeplineStack extends cdk.Stack {
  /**
   *
   * @param {cdk.Construct} scope
   * @param {string} id
   * @param {cdk.StackProps=} props
   */
  constructor(scope, id, props) {
    super(scope, id, props);

    // Allow codebuild admin access to my account. This could be narrow down in future.
    const serviceRole = new iam.Role(this, 'MyRole', {
      assumedBy: new iam.ServicePrincipal('codebuild.amazonaws.com'),
    });
    serviceRole.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AdministratorAccess'));
    // Notification topic
    const topic = new sns.Topic(this, 'RMS_PipeplineStackStatus', {
      displayName: 'Bolt-app pipepline status'
    });

    this.create_codepipeline(serviceRole, topic, "devrms")
  }
  // ==================================================================================
  //
  create_codepipeline(serviceRole, topic, rmsEnv) {
    // Create Artifacts
    const rms_testOutput = new codepipeline.Artifact('RMSTestOutput');
    const builderSourceOutput = new codepipeline.Artifact('RMSTestOutput_fromS3');
    const s3Bucket = new bucket.Bucket(this, 'ikea-rms-chithu-test-trigger', {
      bucketName: 'ikea-rms-chithu-test-trigger',
      versioned: true
    })

    const pipeline = new codepipeline.Pipeline(this, "RMS_BuildPipepline_" + rmsEnv, {
      pipelineName: 'RMS_BuildPipepline_' + rmsEnv,
      crossAccountKeys: false,
    });

    pipeline.addStage({
      stageName: "Source",
      actions: [
        new codepipeline_actions.S3SourceAction({
          actionName: 'S3Trigger',
          bucket: s3Bucket,
          bucketKey: 'rms-functional-tests.zip',
          output: builderSourceOutput,
          trigger: codepipeline_actions.S3Trigger.NONE //Rather not do this

        }),
      ],
    });

    pipeline.addStage({
      stageName: 'Test',
      actions: [
        new codepipeline_actions.CodeBuildAction({
          actionName: 'TestRMS_',
          project: this.RMS_Test(serviceRole, rmsEnv),
          input: builderSourceOutput,
          outputs: [rms_testOutput],
        })
      ],
    })

    new PipelineNotificationRule(this, 'PipelineNotificationRule_' + rmsEnv, {
      name: 'pipeline-notification_' + rmsEnv,
      pipeline,
      events: [
        PipelineEvent.PIPELINE_EXECUTION_STARTED,
        PipelineEvent.PIPELINE_EXECUTION_FAILED,
        PipelineEvent.PIPELINE_EXECUTION_SUCCEEDED,
      ],
      targets: [
        new SnsTopic(topic),
      ],
    });
  }

  // ==================================================================================
  //
  RMS_Test(serviceRole, rmsEnv) {
    return new codebuild.PipelineProject(this, 'rms_' + rmsEnv, {
      projectName: 'rms_' + rmsEnv,
      role: serviceRole,
      buildSpec: codebuild.BuildSpec.fromObject({
        version: '0.2',
        env: {
          shell: 'bash'
        },
        phases: {
          build: {
            commands: [
              // Use CODEBUILD_START_TIME as unique device name
              'cd rms-functional-tests',
              'npm ci',
              'npm run test',
            ],
          },
        },
        reports: {
          jest: {
            files: [
              "**/functional-tests.xml"
            ],
            "file-format": "JunitXml",
            "base-directory": "rms-functional-tests/reports"
          }
        }
      }),
      environment: {
        buildImage: codebuild.LinuxBuildImage.fromAsset(this, 'custom-image-rms-tester_' + rmsEnv, {
          directory: './rms_tester'
        })
      },
    });
  }
}

module.exports = { RmsTestPipeplineStack }
