const aws = require('aws-sdk');
const config = require('../../config');
const { Endpoint } = require('../../utils/Endpoint');
const expectStatusForExecution = require('../../utils/expectStatusForExecution');
const listExecutionsForStateMachine = require('../../utils/listExecutionsForStateMachine');
const { setSuccess, setFailure } = require('../../utils/failure-injection');

describe('ArticleComposer', () => {
    const servicePrefix = 'ArticleComposer';
    const imageID = '1645';
    const bodyParams = {
        imageID,
        callbackUrl: `https://3wmec7bbhl.execute-api.eu-west-1.amazonaws.com/devrms/composer/${imageID}/ArticleComposer`,
        imageKeyPrefix: `image_${imageID}/ArticleComposer/`,
        StartTimestamp: Date.now(),
        systemVersion: `image_${imageID},devrms`,
        logStream: `devrms/image_${imageID}`,
    };

    const headerParams = {
        'x-kp-manifest': `image_${imageID}/manifest.json`,
    }
    const stateMachineArn = 'arn:aws:states:eu-west-1:685380438071:stateMachine:ArticleComposerStepFunction-jonmat';
    const endpoint = new Endpoint(servicePrefix, 'start', null, null, bodyParams, headerParams);
    
    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe.skip('fetchRetailUnits handler is configured for failure', () => {

        it('should fail', async (callback) => {
            await setFailure('articlecomposer-jonmat-fetchretailunits');

            const result = await endpoint.lambda();
            console.log(result);
            expect(result.StatusCode).toBe(200);
            expect(result.Payload).toEqual(JSON.stringify({ success: 'process started' }));

            const executions = await listExecutionsForStateMachine(stateMachineArn)
            expect(executions.length).toBeGreaterThan(0);

            executionArn = executions[0].executionArn;
            expect(executionArn).toBeDefined();
            expectStatusForExecution(executionArn, 'FAILED', callback);
        }, config.timeout * 2);
    });

    describe('fetchRetailUnits handler is NOT configured for failure', () => {

        it('should not fail', async (callback) => {
            await setSuccess('articlecomposer-jonmat-fetchretailunits');
            const result = await endpoint.lambda();
            console.log(result);
            expect(result.StatusCode).toBe(200);
            expect(result.Payload).toEqual(JSON.stringify({ success: 'process started' }));

            const executions = await listExecutionsForStateMachine(stateMachineArn)
            expect(executions.length).toBeGreaterThan(0);

            executionArn = executions[0].executionArn;
            expect(executionArn).toBeDefined();
            expectStatusForExecution(executionArn, 'SUCCEEDED', callback, 10_000);
        }, 60_000 * 3);
    });
});