const aws = require('aws-sdk');
const { SSMClient, PutParameterCommand } = require("@aws-sdk/client-ssm");
const config = require('../../config');
const { Endpoint } = require('../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../lib/lambdas/verification');
const { setSuccess, setFailure } = require('../../utils/failure-injection');

xdescribe('Failure injection in Model3DService', () => {
    const servicePrefix = 'Model3D';
    const endpoint = new Endpoint(servicePrefix, 'find', { name: '00011536', getMeta: 'true', searchType: 'include', clearCache: 'false' })

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('find handler is configured for failure', () => {

        it('should fail', async () => {
            await setFailure('model3dservice-jonmat-find');

            const result = await endpoint.lambda();
            expect(result.StatusCode).toBe(200);
            expect('FunctionError' in result).toBe(true)
        }, config.timeout);
    });

    describe('find handler is NOT configured for failure', () => {

        it('should not fail', async () => {
            await setSuccess('model3dservice-jonmat-find');
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
    });
});