const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('ApplicationParameterService', () => {

    const servicePrefix = 'AppParam';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getApplicationParameter', () => {
        const endpoint = new Endpoint(servicePrefix, 'getApplicationParameter', { id: 1 })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);


        it('should return an applicationParameter', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: expect.any(Number),
                            isDeleted: expect.any(Boolean),
                            parameterKey: expect.any(String),
                            searchType: expect.any(String),
                            retailUnits: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getAllApplicationParametera', () => {
        const endpoint = new Endpoint(servicePrefix, 'getAllApplicationParameters', {})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
        
        it('should return all applicationParameters', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            id: expect.any(Number),
                            isDeleted: expect.any(Boolean),
                            parameterKey: expect.any(String),
                            searchType: expect.any(String),
                        })])
                );
            });
        }, config.timeout);
    });
});