const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');


describe('PatchService', () => {
    const servicePrefix = 'Patch';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getPatch', () => {

        const endpoint = new Endpoint(servicePrefix, 'getPatch', { retailUnit: 'GB', productNumber: 'ART-20340560' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        // Disabled: Patches no longer exists for this productNumber
        xit('should return patches for a product', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                id: expect.any(Number),
                                dateFrom: expect.any(String),
                                dateTo: expect.any(String),
                                productNumber: 'ART-20340560',
                                retailUnit: 'GB',
                            })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('getScpPatches', () => {
        const endpoint = new Endpoint(servicePrefix, 'getScpPatches', { retailUnit: 'GB', productNumber: 'ART-20340560' })


        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
        
        it('should return patches for a Scalabale Product', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            s3key: expect.any(String),
                            bucket: expect.any(String),
                        })
                );
            });
        }, config.timeout);
    });
});