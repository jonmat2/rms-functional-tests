const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');
const { KP_DATA_PROVIDER } = require('../../../lib/constants').SERVICE_PREFIXES;


describe('KpDataProvider', () => {
    const servicePrefix = KP_DATA_PROVIDER;

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('listDesignInfo', () => {

        const endpoint = new Endpoint(servicePrefix, 'listDesignInfo', {});
        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);


        it('should return a list of design info', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            familyName: expect.any(String),
                            designCodes: expect.any(Array),
                        })]
                ));
            });
        }, config.timeout);
    });

    describe('listAllProducts', () => {

        const endpoint = new Endpoint(servicePrefix, 'listAllProducts', {
            productTypes: 'ART',
            retailUnit: 'GB',
        });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of products for a retail unit', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            schemaVersion: expect.any(String),
                            retailUnit: 'GB',
                            productType: 'ART',
                            globalIdNumber: expect.any(String),
                            localIdNumber: expect.any(String),
                            id: expect.any(String),
                        })]
                ));
            });
        }, config.timeout);
    })

    describe('getFileUrls', () => {
        const endpoint = new Endpoint(servicePrefix, 'getFileUrls', { retailUnit: 'GB', productId: 'empty' });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the .bm3 file url for a retail unit', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            bm3: expect.any(String)
                        })
                );
            });
        }, config.timeout);
    });

    describe('getPrices', () => {
        const endpoint = new Endpoint(servicePrefix, 'prices', { retailUnit: 'GB' });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
        it('should return prices for a retail unit', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            schemaVersion: expect.any(String),
                            retailUnit: 'GB',
                            globalIdNumber: expect.any(String),
                            localIdNumber: expect.any(String),
                            id: expect.any(String),
                            price: expect.objectContaining({
                                pricingMethod: expect.any(String),
                                priceRoundingMethod: expect.any(String),
                                fees: expect.any(Array),
                                prices: expect.arrayContaining([
                                    expect.objectContaining({
                                        currency: expect.any(String),
                                        discountType: expect.any(String),
                                        price: expect.any(Number)
                                    })
                                ]),
                                notPricedOnFrontEdge: expect.any(Boolean)
                            })
                        })]
                ));
            });
        }, config.timeout);
    });

    describe('hydrateProducts', () => {
        const endpoint = new Endpoint(servicePrefix, 'hydrateProducts', {
            origin: 'source',
            retailUnit: 'GB',
            ids: 'ART-60368806',
        });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of hydrated products', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            schemaVersion: expect.any(String),
                            retailUnit: 'GB',
                            globalIdNumber: expect.any(String),
                            localIdNumber: expect.any(String),
                            id: expect.any(String),
                            commercial: expect.any(Object),
                            classification: expect.any(Object),
                            assets: expect.any(Object),
                            behaviors: expect.any(Object),
                            patches: expect.any(Array),
                            price: expect.objectContaining({
                                pricingMethod: expect.any(String),
                                priceRoundingMethod: expect.any(String),
                                fees: expect.any(Array),
                                prices: expect.arrayContaining([
                                    expect.objectContaining({
                                        currency: expect.any(String),
                                        discountType: expect.any(String),
                                        price: expect.any(Number)
                                    })
                                ]),
                                notPricedOnFrontEdge: expect.any(Boolean)
                            })
                        })]
                ));
            });
        }, config.timeout);
    });

    describe('getHydratedScp', () => {
        const endpoint = new Endpoint(servicePrefix, 'getHydratedScp', {
            retailUnit: 'GB',
            ids: 'SCP-121_veddinge_front.bm3-10935-width-height',
            environment: 'RmsDevRange',
        });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of hydrated Scp`s', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(expect.arrayContaining(
                    [expect.objectContaining(
                        {
                            schemaVersion: expect.any(String),
                            retailUnit: 'GB',
                            globalIdNumber: expect.any(String),
                            localIdNumber: expect.any(String),
                            id: expect.any(String),
                            commercial: expect.any(Object),
                            classification: expect.any(Object),
                            assets: expect.any(Object),
                            behaviors: expect.any(Object),
                            patches: expect.any(Array),
                            price: expect.any(Object),
                        })]
                ));
            });
        }, config.timeout);
    });
});