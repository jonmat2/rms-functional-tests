const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('TemplateCatalogRuleService', () => {
    const servicePrefix = 'TemplateCatalogRule';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getRules', () => {
        const endpoint = new Endpoint(servicePrefix, 'getRules')

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the template catalog rules', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            ID_rule: expect.objectContaining({
                                Schema: expect.any(Object),
                                Constraint: expect.any(Object),
                            }),
                        })
                );
            });
        }, config.timeout);
    });
});