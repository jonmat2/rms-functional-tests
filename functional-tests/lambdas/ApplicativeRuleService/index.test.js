const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('ApplicativeRuleService', () => {
    const servicePrefix = 'ApplicativeRule';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('listApplicativeRules', () => {

        const endpoint = new Endpoint(servicePrefix, 'listApplicativeRules', {});
        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of applicativeRules', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                ruleKey: expect.any(String),
                                specification: expect.any(Object),
                                items: expect.any(Array),
                            })
                    ]));
            });
        }, config.timeout);
    });
});