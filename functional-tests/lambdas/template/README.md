# TEMPLATE

The index.test.js_ file describes the typical boilerplate code for a test executing and then making assertions on a lambda. It can be used as a template when creating new tests.