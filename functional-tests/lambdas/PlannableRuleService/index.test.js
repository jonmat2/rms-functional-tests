const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('PlannableRuleService', () => {
    const servicePrefix = 'PlannableRule';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getPlannable', () => {
        const endpoint = new Endpoint(servicePrefix, 'getPlannable', {})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);


        it('should return a list of plannable rules', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            'typecoderule-11153-SCP': expect.objectContaining({
                                Plannable: expect.any(Boolean),
                                Schema: expect.any(Object),
                                Conditions: expect.any(Object),
                            }),
                        })
                );
            });
        }, config.timeout);
    });
});