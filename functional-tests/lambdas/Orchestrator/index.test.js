const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('Orchestrator', () => {
    const servicePrefix = 'Orchestrator';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('status', () => {

        const endpoint = new Endpoint(servicePrefix, 'status', null, { imageID: '1176' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return status for an image', async () => {
            await verifyPayload(endpoint, (payload) => {
                payload = JSON.parse(payload); // This payload is overstringified for some reason

                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            ImageID: 1176,
                            StartTimestamp: expect.any(Number),
                            PublicationLog: expect.any(String),
                            Valid: expect.any(Boolean),
                            ImageCompositionStatus: expect.any(String),
                         })
                );
            });
        }, config.timeout);
    });

    describe('images', () => {

        const endpoint = new Endpoint(servicePrefix, 'images', { pageSize: 10 })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should list images', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            totalNumberOfImages: expect.any(Number),
                            images: expect.any(Array),
                        }
                    ));
            });
        }, config.timeout);
    });

    describe('logs', () => {
        const endpoint = new Endpoint(servicePrefix, 'logs', null, { imageID: '1177', severity: 'error' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should list logs for an existing image', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            signedUrl: expect.any(String),
                        }
                    ));
            });
        }, config.timeout);
    });

});