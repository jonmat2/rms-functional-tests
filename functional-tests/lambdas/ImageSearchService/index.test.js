const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('ImageSearchService', () => {
    const servicePrefix = 'ImageSearchService';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('imageIndexingQueueStats', () => {

        const endpoint = new Endpoint(servicePrefix, 'imageIndexingQueueStats', {});
        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return statistics for the ImageIndexingQueue', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            queueLength: expect.any(Number),
                        })
                );
            });
        }, config.timeout);
    });
});