const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');


describe('PlannerConnectorService', () => {
    const servicePrefix = 'PlannerConnectorService';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getStyles', () => {
        const endpoint = new Endpoint(servicePrefix, 'getStyles', null, { environment: 'RmsDevRange', appDist: 'GB' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of kitchen styles', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            NeedMapping: { Modern: expect.any(Object), Traditional: expect.any(Object) },
                            Default: expect.any(Object)
                        })
                );
            });
        }, config.timeout);
    });

    describe('listStyles', () => {
        const endpoint = new Endpoint(servicePrefix, 'listStyles', null, { environment: 'test'})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a list of kitchen styles files', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining(['GB'])
                );
            });
        }, config.timeout);
    });
});