const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('Model3DService', () => {
    const servicePrefix = 'Model3D';


    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('find', () => {

        const endpoint = new Endpoint(servicePrefix, 'find', { name: '00011536', getMeta: 'true', searchType: 'include', clearCache: 'false' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a 3D model', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                name: '00011536',
                                type: expect.any(String),
                                file: expect.any(String),
                                url: expect.any(String)
                            })
                    ])
                );
            });
        }, config.timeout);
    });
});