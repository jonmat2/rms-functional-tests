const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');


describe('ProductGraphService', () => {
    const servicePrefix = 'ProductGraph';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getLockTree', () => {
        const endpoint = new Endpoint(servicePrefix, 'getLockTree', { actor: 'ASL-42460282', retailUnit: 'GB', state: 'development' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the lock tree for an actor', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            actor: 'ASL-42460282',
                            retailUnit: 'GB',
                            roots: expect.any(Array),
                            tree: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });
});