const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('RetailUnitConfigService', () => {
    const servicePrefix = 'RetailUnitConfig';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getRetailUnitConfig', () => {
        const endpoint = new Endpoint(servicePrefix, 'getRetailUnitConfig', { id: 'GB' })


        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the retailunit configuration for a given retailunit', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: 'GB',
                            name: 'GB',
                            locales: expect.any(Array),
                            navigation: expect.any(Object),
                            defaultStyle: expect.any(Object),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getRetailUnits', () => {
        const endpoint = new Endpoint(servicePrefix, 'getRetailUnits', {})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the retailunits', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                code: expect.any(String),
                                name: expect.any(String),
                                regionGroup: expect.any(String),
                            })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('getEditorConfig', () => {
        const endpoint = new Endpoint(servicePrefix, 'getEditorConfig', { id: 'TemplateMaster' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the retailunit config editor', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: expect.any(String),
                            name: expect.any(String),
                            locales: expect.any(Array),
                            navigation: expect.any(Object),
                            defaultStyle: expect.any(Object),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getRetailUnitConfigState', () => {
        const endpoint = new Endpoint(servicePrefix, 'getRetailUnitConfigState', { id: 'GB' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return the retailunit config state', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            retailUnit: 'GB',
                            state: expect.any(String),
                        })
                );
            });
        }, config.timeout);
    });
});