const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

// Work in progress
describe.skip('ProductEnvironmentProposal', () => {
    const servicePrefix = 'ProductEnvironmentProposal';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getEnvironments', () => {

        const endpoint = new Endpoint(servicePrefix, 'getEnvironments', {
            callbackUrl: 'https://rms.url/', imageID: '10', publicationType: 'quick'
        });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);


        // Complaints about the arguments even though they seem to be the expected.
        it('should return environments, retail units and latest publication products', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            forceAssetRepublish: expect.any(Boolean),
                            systemVersion: expect.any(String),
                        }
                    ));
            });
        }, config.timeout);
    });
});