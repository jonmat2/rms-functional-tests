const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('PublishingLogService', () => {
    const servicePrefix = 'PublishingLog';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    // Disabled: Request times out
    xdescribe('queryLog', () => {
        const endpoint = new Endpoint(servicePrefix, 'queryLog', {imageID: '1416'})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout * 4);

        it('should list logs for a publish', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            totalNumberOfItems: expect.any(Number),
                            next: expect.any(String),
                            result: expect.any(Array),
                        })
                );
            });
        }, config.timeout * 4);
    });
});