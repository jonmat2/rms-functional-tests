const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');

describe('RangeSchemaSubSystem', () => {
    const servicePrefix = 'RangeSchema';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('getAllSchemaParameters', () => {
        const endpoint = new Endpoint(servicePrefix, 'getAllSchemaParameters', { page: 0 })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return all schema parameters', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            totalItemCount: expect.any(Number),
                            result: expect.arrayContaining([{
                                id: expect.any(Number),
                                name: expect.any(String),
                                parameterKey: expect.any(String),
                                translationKey: expect.any(String),
                            }])
                        })
                );
            });
        }, config.timeout);
    });

    describe('getSchemaParameterNamesAndIds', () => {
        const endpoint = new Endpoint(servicePrefix, 'getSchemaParameterNamesAndIds', {})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return names & ids for schema parameters', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        {
                            id: expect.any(Number),
                            name: expect.any(String),
                        }
                    ])
                );
            });
        }, config.timeout);
    });

    describe('querySchemaParameters', () => {
        const endpoint = new Endpoint(servicePrefix, 'querySchemaParameters', { rql: 'id:518 OR id:223', page: 0 })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should support RQL for querying for paginated schema parameters', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            totalItemCount: expect.any(Number),
                            result: expect.arrayContaining([{
                                id: expect.any(Number),
                                name: expect.any(String),
                                parameterKey: expect.any(String),
                                translationKey: expect.any(String),
                            }])
                        })
                );
            });
        }, config.timeout);
    });

    describe('getSchemaParameter', () => {
        const endpoint = new Endpoint(servicePrefix, 'getSchemaParameter', { parameterKey: 'leg' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a schema parameter by id or parameterKey', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: expect.any(Number),
                            name: 'leg',
                            parameterKey: 'leg',
                            translationKey: 'leg',
                            schemaParameterValues: expect.any(Array),
                            schemaNames: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getArchetypeParameters', () => {
        const endpoint = new Endpoint(servicePrefix, 'getArchetypeParameters', { name: 'bug_arch' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should get parameters for an Archetype name', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                id: expect.any(Number),
                                name: 'leg',
                                parameterKey: 'leg',
                                translationKey: 'leg',
                                schemaParameterValues: expect.any(Array),
                            })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('searchSchemasAndArchetypes', () => {

        const endpoint = new Endpoint(servicePrefix, 'searchSchemasAndArchetypes', { rql: 'id:1 OR id:2', next: '0,0' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should find schemas and archetypes using RQL', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            totalItemCount: expect.any(Number),
                            next: expect.any(String),
                            result: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getArchetype', () => {
        const endpoint = new Endpoint(servicePrefix, 'getArchetype', { archetypeName: 'bug_arch' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return an archetype having the specified name', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: expect.any(Number),
                            name: 'bug_arch',
                            type: 'archetype',
                            schemaParameterValues: expect.any(Array),
                            schemaParameterConfigs: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getSchema', () => {

        const endpoint = new Endpoint(servicePrefix, 'getSchema', { schemaName: 'bug_schema' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return a schema having the specified name', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: expect.any(Number),
                            name: 'bug_schema',
                            type: 'schema',
                            schemaParameterValues: expect.any(Array),
                            schemaParameterConfigs: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getProductSchemaParameters', () => {

        const endpoint = new Endpoint(servicePrefix, 'getProductSchemaParameters', { schemaName: 'bug_schema' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return schema parameters for a schema name', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                leg: expect.objectContaining({
                                    typeID: expect.any(Number),
                                    translation: expect.any(Object),
                                    ids: expect.any(Array),
                                }),
                            })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('getProductsByArchetype', () => {
        const endpoint = new Endpoint(servicePrefix, 'getProductsByArchetype', { archetypeName: 'CISUB2000' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return all products for a given archetype', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            result: expect.any(Array),
                        })
                );
            });
        }, config.timeout);
    });

    describe('getResolvedSchemaParameters', () => {

        const endpoint = new Endpoint(servicePrefix, 'getResolvedSchemaParameters',
            { typeCode: 'CITOP1353', productId: 'ASL-42460303', retailUnit: 'GB' })

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return all resolved schema parameters for a given product', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            parameterKey: expect.any(String),
                        }),
                        expect.objectContaining({
                            readable: expect.any(Boolean),
                            writeable: expect.any(Boolean),
                            nullable: expect.any(Boolean),
                        })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('GetSchemaParametersForTypecode', () => {
        const endpoint = new Endpoint(servicePrefix, 'GetSchemaParametersForTypecode',
        { typeCode: 'CITOP1353' });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('should return parameters for a given typecode', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            parameterKey: expect.any(String),
                        }),
                        expect.objectContaining({
                            readable: expect.any(Boolean),
                            writeable: expect.any(Boolean),
                            nullable: expect.any(Boolean),
                        })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('getSchemaForTypeCode', () => {
        
        const endpoint = new Endpoint(servicePrefix, 'getSchemaForTypeCode',
        { typeCode: 'CITOP1353' });

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
        it('should return the schema for a given typecode', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining({
                        hasProductInformationPopUp: expect.any(Boolean),
                        parameters: expect.arrayContaining([
                            expect.objectContaining({
                                id: expect.any(Number),
                                name: expect.any(String),
                                parameterKey: expect.any(String),
                            })]),
                    })
                );
            });
        }, config.timeout);
    });

});
