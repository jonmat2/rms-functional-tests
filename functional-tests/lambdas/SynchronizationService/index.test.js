const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const { verifySuccessfulInvocation, verifyPayload } = require('../../../lib/lambdas/verification');


describe('SynchronizationService', () => {
    const servicePrefix = 'Synchronization';

    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('environments', () => {
        const endpoint = new Endpoint(servicePrefix, 'environments', {})

        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);

        it('shoud list environments supporting synchronization', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining(
                            {
                                environment: expect.any(String),
                            })
                    ])
                );
            });
        }, config.timeout);
    });

    describe('getSynchronization', () => {
        const endpoint = new Endpoint(servicePrefix, 'getSynchronization', {}, { ID: 182 })
        it('should not report any errors', async () => {
            await verifySuccessfulInvocation(endpoint);
        }, config.timeout);
      
        it('should return a synchronization by a given id', async () => {
            await verifyPayload(endpoint, (payload) => {
                expect(payload).toEqual(
                    expect.objectContaining(
                        {
                            id: 182,
                            createDate: expect.any(String),
                            status: expect.any(Number),
    
                        })
                );
            });
        }, config.timeout);
    });
});