const aws = require('aws-sdk');
const config = require('../../../config');
const { Endpoint } = require('../../../lib/lambdas/Endpoint');
const expectStatusForExecution = require('../../../lib/step-functions/expectStatusForExecution');
const listExecutionsForStateMachine = require('../../../lib/step-functions/listExecutionsForStateMachine');

describe('ArticleComposer', () => {
    const servicePrefix = 'ArticleComposer';
    const imageID = '1900';
    const bodyParams = {
        imageID,
        callbackUrl: `https://3wmec7bbhl.execute-api.eu-west-1.amazonaws.com/devrms/composer/${imageID}/ArticleComposer`,
        imageKeyPrefix: `image_${imageID}/ArticleComposer/`,
        StartTimestamp: Date.now(),
        systemVersion: `image_${imageID},devrms`,
        logStream: `devrms/image_${imageID}`,
    };

    const headerParams = {
        'x-kp-manifest': `image_${imageID}/manifest.json`,
    }
    const stateMachineArn = 'arn:aws:states:eu-west-1:685380438071:stateMachine:ArticleComposerStepFunction-devrms';
    const endpoint = new Endpoint(servicePrefix, 'start', null, null, bodyParams, headerParams);
    
    beforeAll(() => {
        aws.config.update({ region: config.region });
    });

    describe('fetchRetailUnits', () => {

        it('should succeed', async () => {
            const result = await endpoint.lambda();
            console.log(result);
            expect(result.StatusCode).toBe(200);
            expect(result.Payload).toEqual(JSON.stringify({ success: 'process started' }));

            const executions = await listExecutionsForStateMachine(stateMachineArn)
            expect(executions.length).toBeGreaterThan(0);

            executionArn = executions[0].executionArn;
            expect(executionArn).toBeDefined();
            const executionResult = await expectStatusForExecution(executionArn, 'SUCCEEDED');
            expect(executionResult.success).toBe(true);
        }, 60_000 * 3);
    });
});