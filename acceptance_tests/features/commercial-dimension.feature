Feature: Use the commercial dimensions from IKEA source system

As a user, when I am reviewing a product that has the respective dimensions data in the 
IKEA source system, I can read the localized information in the planner, in the product card, 
product info pop-up, and the item list.

The textMetric and textImperial fields are from measureReference data in DEXF

Please note: The Current implementation only support one locales where the first one is alway used.


  Background:
    Given a product "ART-14630009" with textMetric as "120x75 cm" and textImperial as "47 1/4x29 1/2 ""
    And a product "ART-80163804" without textMetric or textImperial
    And a retail unit configuration "GB" with first locales where the unitSystem is set to "metrics"
    And a retail unit configuration "CA" with first locales where the unitSystem is set to "imperial"    
    
    Scenario Outline: View commercial dimensions on product in RMS
      When <Product> is retrieve from the pKDataProvider for <RetailUnit> 
      Then parameter commencialDimensions should be <Value>
    
      Examples:

      | Product         | RetailUnit | Value               |
      | "ART-14630009"  |     "GB"   | '120x75 cm'         |
      | "ART-80163804"  |     "GB"   | ''                  |
      | "ART-14630009"  |     "CA"   | '47 1/4x29 1/2 "'   |
      | "ART-80163804"  |     "CA"   | ''                  | 
