const { Given, Then, When } = require('@cucumber/cucumber');
const PasswordValidator = require('./password-validator');
var chai = require("chai");
var expect = chai.expect;

let passwordValidator = new PasswordValidator();
let accessGranted = false;

Given('I have previously created a password', function () {
    passwordValidator.setPassword('1234');
});

When('I enter my password correctly', function () {
    accessGranted = passwordValidator.validatePassword('1234');
});

Then('I should be granted access', function () {
    expect(accessGranted).to.be.true;
});
