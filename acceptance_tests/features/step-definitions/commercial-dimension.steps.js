const { Given, Then, When, setDefaultTimeout } = require('@cucumber/cucumber');
const fetch = require('node-fetch');
var chai = require("chai");
var expect = chai.expect;

const { Endpoint } = require('../../../utils/Endpoint');

setDefaultTimeout(10000)

Given(/^a product "([^"]*)" with textMetric as "([^"]*)" and textImperial as "([^"]*")"/, async (itemId, textMetric, textImperial) => {
    let response = await fetch(`https://api.dexf.qa.ikeadt.com/product/v1/query?fields=*&filter.itemId=${itemId}`, {
        "headers": {
            "DEXF-API-KEY": "7cf09af3-1e5c-4420-9455-297be52deac2"
        }
    });

    expect(response.ok).is.true
    let json = await response.json();
    expect(json.data[0].itemId).to.equal(itemId)
    expect(json.data[0].content.measureReference.textMetric).to.equal(textMetric)
    expect(json.data[0].content.measureReference.textImperial).to.equal(textImperial)
});

Given('a product {string} without textMetric or textImperial', async function (itemId) {

    let response = await fetch(`https://api.dexf.qa.ikeadt.com/product/v1/query?fields=*&filter.itemId=${itemId}`, {
        "headers": {
            "DEXF-API-KEY": "7cf09af3-1e5c-4420-9455-297be52deac2"
        }
    });

    expect(response.ok).is.true
    let json = await response.json();
    expect(json.data[0].itemId).to.equal(itemId)
    expect(json.data[0].content.measureReference).to.be.undefined
});

Given('a retail unit configuration {string} with first locales where the unitSystem is set to {string}', async function (retailUnit, unitSystem) {

    const endpoint = new Endpoint('RetailUnitConfig', 'getRetailUnitConfig', { id: retailUnit })
    const result = await endpoint.lambda();

    expect(result.StatusCode).to.equal(200);

    const payload = JSON.parse(result.Payload);
    expect(payload.locales.length).is.greaterThan(0)
    expect(payload.locales[0].unitSystem).to.equal(unitSystem)
});

Given('I successfully done a new publishing', function () {
    // Assumed done manually before run this test
});

When('I view the {string} with the {string}', function (itemId, retailUnit) {

    return 'pending';
});

Then('I should see the {string} in the planner', function (string) {

    return 'pending';
});

Then('I should see the {string}" in the planner', function (string) {

    return 'pending';
});

When('{string} is retrieve from the pKDataProvider for {string}', async function (itemId, retailUnit) {
    const endpoint = new Endpoint('KpDataProvider', 'hydrateProducts',
        { ids: itemId, retailUnit: retailUnit, includePatches: 'true', origin: 'source' }, null, 'chithu')
    const result = await endpoint.lambda();

    expect(result.StatusCode).to.equal(200);

    const payload = JSON.parse(result.Payload);
    expect(payload.message).is.undefined
    this.commercialDimensions = payload[0].classification.parameters.value.definition.commercialDimensions   
});

// Then(/^parameter commencialDimensions should be '([^']*)$/, function (value) {
Then('parameter commencialDimensions should be {string}', function (value) {
    expect(this.commercialDimensions.values[0]).equal(value)
});

Then('parameter commencialDimensions with {string} should be presented to user', function (value) {
    return 'pending';
});

Given('the product {string} is cached in pKDataProvider', async function (itemId) {


});

When('retail unit {string} and {string} are selected', function (string, string2) {

    return 'pending';
});

When('retail unit {string} is selected', function (string) {
    return 'pending';
});

When('user is reviewing product {string} in the RMS', function (string) {

    return 'pending';
});

Then('the parameter is read only', function () {

    return 'pending';
});