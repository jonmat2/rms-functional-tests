
const { SSMClient, PutParameterCommand } = require("@aws-sdk/client-ssm");

const getFailureConfig = () => JSON.stringify({ isEnabled: true, failureMode: "exception", rate: 1, exceptionMsg: "This lambda is configured for failure!", "statusCode": 404 });
const getSuccessConfig = () => JSON.stringify({ isEnabled: false });

const putParameter = async input => {
    const client = new SSMClient({});
    const command = new PutParameterCommand(input);
    await client.send(command);
}

const setFailure = async name => {
    const input = { Name: name, Overwrite: true, Value: getFailureConfig() };
    await putParameter(input);
}

const setSuccess = async name => {
    const input = { Name: name, Overwrite: true, Value: getSuccessConfig() };
    await putParameter(input);
}

module.exports = { setFailure, setSuccess };