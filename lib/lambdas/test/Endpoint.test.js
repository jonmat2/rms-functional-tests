const { Endpoint } = require("../Endpoint");
const AWS = require('aws-sdk');
const config = require('../../../config');

jest.mock('aws-sdk');

describe('Endpoint', () => {

    afterAll(() => {
        jest.resetAllMocks();
        jest.restoreAllMocks();
    });


    it('should call the lambda with the expected pathParameters', async () => {
        // Arrange
        const mockInvoke = jest.fn(() => ({
            promise: jest.fn(() => Promise.resolve({
                StatusCode: 200,
            })),
        }));

        AWS.Lambda.mockImplementation(() => ({
            invoke: mockInvoke,
        }));

        // Act
        const endpoint = new Endpoint('foo', 'bar', null, { baz: 1 });
        await endpoint.lambda();

        // Assert
        expect(mockInvoke).toHaveBeenCalledWith({
            FunctionName: `foo-${config.stage}-bar`,
            InvocationType: 'RequestResponse',
            Payload: expect.stringContaining('"pathParameters":{"baz":1}')
        });
    })

    it('should call the lambda with the expected queryParameters', async () => {
        // Arrange
        const mockInvoke = jest.fn(() => ({
            promise: jest.fn(() => Promise.resolve({
                StatusCode: 200,
            })),
        }));

        AWS.Lambda.mockImplementation(() => ({
            invoke: mockInvoke,
        }));

        // Act
        const endpoint = new Endpoint('foo', 'bar', { baz: 1 } );
        await endpoint.lambda();

        // Assert
        expect(mockInvoke).toHaveBeenCalledWith({
            FunctionName: `foo-${config.stage}-bar`,
            InvocationType: 'RequestResponse',
            Payload: expect.stringContaining('"queryStringParameters":{"baz":1}'),
            Payload: expect.stringContaining('"multiValueQueryStringParameters":{"baz":[1]}')
        });
    })

    it('should create the expected MultiValueQueryStringParameters', () => {
        const result = new Endpoint()._getMultiValueQueryStringParameters({ foo: 'bar', baz: 1 });
        expect(result).toEqual({ foo: ['bar'], baz: [1] });
    })
});