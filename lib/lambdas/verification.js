/**
    * Verifies that the enpdoint is called without errors
    *
    * @param {Endpoint} endpoint the service prefix for the service being tested
    * @returns {null}
    */
const verifySuccessfulInvocation = async (endpoint) => {
    const result = await endpoint.lambda();
    expect(result.StatusCode).toBe(200);
    expect('FunctionError' in result).toBe(false)
}

/**
    * Verifies that the enpdoint returns the expected payload
    *
    * @param {Endpoint} endpoint the service prefix for the service being tested
    * @param {function(payload: Object)} verificationFn function with expectations
    * @returns {null}
    */
const verifyPayload = async (endpoint, verificationFn) => {
    const result = await endpoint.lambda();
    expect(result.Payload).not.toBeUndefined();

    const payload = JSON.parse(result.Payload);

    verificationFn(payload);
}

module.exports = { verifySuccessfulInvocation, verifyPayload }