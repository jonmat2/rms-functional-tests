const aws = require('aws-sdk');
const config = require('../../config');

/**
 * Utility class to invoke Endpoints
 */
class Endpoint {

    /**
    * Creates an endpoint that can be invoked as a lambda and eventually using rest.
    *
    * @param {string} servicePrefix the service prefix for the service being tested
    * @param {string} functionName the name of the function 
    * @param {Object} queryStringParameters parameters to be supplied as a querysting
    * @param {Object} pathParameters parameters to be supplied in the path
    * @param {Object} body the reuquest body
    * @param {Object} headerParameters parameters to be added to the header
    */
    constructor(servicePrefix, functionName, queryStringParameters, pathParameters, body, headerParameters) {
        this.servicePrefix = servicePrefix;
        this.functionName = functionName;
        this.queryStringParameters = queryStringParameters;
        this.pathParameters = pathParameters;
        this.body = body;
        this.headerParameters = headerParameters;
    }

    _getMultiValueQueryStringParameters = (params) => {
        let result = {};
        Object.entries(params).forEach(([key, value]) => {
            result = { ...result, [key]: [value] }
        });
        return result;
    };

    _getServiceStageOverride(servicePrefix) {
        if (config.serviceStageOverrides.hasOwnProperty(servicePrefix)) {
            return config.serviceStageOverrides[servicePrefix];
        }
        return null;
    }

    rest() {
        throw new Error('Not implemented');
    }

    /**
    * Executes the lambda
    *
    * @returns {Promise<PromiseResult<aws.Lambda.InvocationResponse, aws.AWSError>>}
    */
    lambda() {
        const stage = this._getServiceStageOverride(this.servicePrefix) || config.stage; 
        const lambdaName = `${this.servicePrefix}-${stage}-${this.functionName}`
        // console.log(`Calling lambda '${lambdaName}'`)
        const lambda = new aws.Lambda();
        return lambda.invoke({
            FunctionName: lambdaName,
            InvocationType: 'RequestResponse',
            Payload: JSON.stringify({
                queryStringParameters: this.queryStringParameters || {},
                multiValueQueryStringParameters: this.queryStringParameters && this._getMultiValueQueryStringParameters(this.queryStringParameters),
                pathParameters: this.pathParameters || {},
                body: JSON.stringify(this.body),
                headers: {
                    'x-kp-system-version': config.stage,
                    'x-kp-cache-date': Date.now(),
                    ...this.headerParameters,
                },
            }),
        }).promise();
    }
}

module.exports = {
    Endpoint,
};