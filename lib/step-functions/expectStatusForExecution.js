const StepFunctions = require('aws-sdk/clients/stepfunctions');

const expectStatusForExecution = async (executionArn, expectedStatus, pollInterval = 5_000) => {
    const pollForExecutionStatus = async () => {
        const params = {
            executionArn,
        }
        const stepFunctions = new StepFunctions();
        const { status } = await stepFunctions.describeExecution(params).promise();
        const ms = Date.now() - start;
        console.log(`Polling after ${Math.floor(ms / 1000)} seconds and recieved status: ${status}`);
        if (status !== 'RUNNING') {
            console.log(`Execution stopped with status: ${status}`);
            return { done: true, success: status === expectedStatus };
        }
        return { done: false };
    };

    const start = Date.now();
    let executionStatus = { done: false, success: false }

    while (!executionStatus.done) {
        executionStatus = await pollForExecutionStatus();
        await new Promise((resolve) => setTimeout(resolve, pollInterval));
    }
    return executionStatus;
}

module.exports = expectStatusForExecution;