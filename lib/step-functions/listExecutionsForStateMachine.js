const StepFunctions = require('aws-sdk/clients/stepfunctions');

const listExecutionsForStateMachine = async (stateMachineArn) => {
    const stepFunctions = new StepFunctions();
    const params = {
        stateMachineArn,
        statusFilter: 'RUNNING'
    }
    const { executions } = await stepFunctions.listExecutions(params).promise();
    return executions;
};

module.exports = listExecutionsForStateMachine;