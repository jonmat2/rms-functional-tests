const SERVICE_PREFIXES = {
    KP_DATA_PROVIDER: 'KpDataProvider',
    IMAGE_SEARCH_SERVICE: 'ImageSearchService',
    MODEL_3D_SERVICE: 'Model3D',
    ARTICLE_COMPOSER: 'ArticleComposer',
}

module.exports = { SERVICE_PREFIXES }